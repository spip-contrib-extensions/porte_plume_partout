<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/ppp?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_mettre_a_jour' => 'Atualizar',

	// C
	'cfg_attention' => 'Atenção',
	'cfg_description' => 'em função dos seus templates, só ative o Porte-Plume nos campos em que o uso dos atalhos não gerará erros de XHTML',
	'cfg_titre' => 'Porte Plume Partout',

	// E
	'explication_personnalisation' => 'Indique o alvo dos elementos que usarão a barra tipográfica (expressão CSS ou extenção jQuery)',

	// L
	'label_hauteur_champ' => 'Meia altura da tela',
	'label_personnalisation' => 'Seletor personalizado',
	'legend_barre_typo' => 'Ativar a barra tipográfica em:',
	'legend_hauteur_champ' => 'Altura do campo texto para as matérias',
	'legend_personnalisation' => 'Personalização',

	// S
	'supprimer' => 'Retornar aos valores padrão'
);

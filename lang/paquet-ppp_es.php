<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-ppp?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'ppp_description' => 'Porta Plumas Por todas partes sirve para añadir la barra de edición a los campos DESCRIPTIF, CHAPO y PS',
	'ppp_nom' => 'Porta Plumas Por todas partes',
	'ppp_slogan' => 'Mostrar el Porta Plumas sobre la mayor parte de los campos de entrada'
);

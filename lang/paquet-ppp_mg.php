<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-ppp?lang_cible=mg
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'ppp_description' => 'Porte Plume Partout sert à ajouter la barre d’édition aux champs DESCRIPTIF, CHAPO et PS',
	'ppp_nom' => 'Porte Plume Partout',
	'ppp_slogan' => 'Afficher le Porte Plume sur la plupart des champs de saisie'
);

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-ppp?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'ppp_description' => 'Portapenne Ovunque viene utilizzato per aggiungere la barra di modifica ai campi DESCRIPTIF, CHAPO e PS',
	'ppp_nom' => 'Portapenne ovunque',
	'ppp_slogan' => 'Visualizza Portapenne sulla maggior parte dei campi di input'
);

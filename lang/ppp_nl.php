<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/ppp?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_mettre_a_jour' => 'Aanpassen',

	// C
	'cfg_attention' => 'Let op',
	'cfg_description' => 'in functie van je skeletten moet je ervoor zorgen Porte Plume niet te openen in velden waarbij het gebruik xhtml-fouten zou kunnen veroorzaken.',
	'cfg_titre' => 'Porte Plume Partout',

	// E
	'explication_personnalisation' => 'Vermeld het kenmerk van de elementen die de  typografische toolbar gebruiken (CSS of uitgebreide jQuery).',

	// L
	'label_hauteur_champ' => 'Halve schermhoogte',
	'label_personnalisation' => 'Persoonlijke keuze',
	'legend_barre_typo' => 'Activeer de typografische knoppenbalk op:',
	'legend_hauteur_champ' => 'Hoogte van het tekstveld voor artikelen',
	'legend_personnalisation' => 'Personalisering',

	// S
	'supprimer' => 'Zet alle standaardwaardes terug'
);

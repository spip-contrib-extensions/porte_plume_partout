<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/ppp?lang_cible=mg
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_mettre_a_jour' => 'Mettre à jour',

	// C
	'cfg_attention' => 'Attention',
	'cfg_description' => 'en fonction de vos squelettes, veillez à n’activer le Porte Plume que sur des champs pour lesquels l’utilisation des raccourcis n’engendrera pas d’erreur xhtml.',
	'cfg_titre' => 'Porte Plume Partout',

	// E
	'explication_personnalisation' => 'Indiquez la cible des éléments qui utiliseront la barre typographique (Expression CSS ou étendue jQuery).',

	// L
	'label_hauteur_champ' => 'Demi-hauteur de l’écran',
	'label_personnalisation' => 'Sélecteur personnalisé',
	'legend_barre_typo' => 'Activer la barre typographique sur :',
	'legend_hauteur_champ' => 'Hauteur du champ texte pour les articles',
	'legend_personnalisation' => 'Personnalisation',

	// S
	'supprimer' => 'Remettre les valeurs par défaut'
);

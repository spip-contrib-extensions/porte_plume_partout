<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-ppp?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'ppp_description' => 'Porte Plume Partout voegt de typografische knoppenbalk toe aan de velden DESCRIPTIF, CHAPO en PS',
	'ppp_nom' => 'Porte Plume Partout',
	'ppp_slogan' => 'Gebruik Porte Plume bij de meeste invoervelden'
);

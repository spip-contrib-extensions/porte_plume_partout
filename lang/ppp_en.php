<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/ppp?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_mettre_a_jour' => 'Update',

	// C
	'cfg_attention' => 'Warning',
	'cfg_description' => 'according to your templates, be sure to only activate the Quill on fields where the use will not create xhtml error.',
	'cfg_titre' => 'Quill everywhere',

	// E
	'explication_personnalisation' => 'Specify the target elements that will use the typographical bar (CSS or jQuery expression).',

	// L
	'label_hauteur_champ' => 'Half size of the screen',
	'label_personnalisation' => 'Custom selector',
	'legend_barre_typo' => 'Enable the editing toolbar on :',
	'legend_hauteur_champ' => 'Height of the text field on articles',
	'legend_personnalisation' => 'Customization',

	// S
	'supprimer' => 'Reset to the default values'
);

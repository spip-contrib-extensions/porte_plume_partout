<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/porte_plume_partout.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'ppp_description' => 'Porte Plume Partout sert à ajouter la barre d’édition aux champs DESCRIPTIF, CHAPO et PS',
	'ppp_nom' => 'Porte Plume Partout',
	'ppp_slogan' => 'Afficher le Porte Plume sur la plupart des champs de saisie'
);

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/ppp?lang_cible=ru
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_mettre_a_jour' => 'Обновить',

	// C
	'cfg_attention' => 'Внимание',
	'cfg_titre' => 'Porte Plume Partout',

	// L
	'legend_barre_typo' => 'Добавить панель инструментов для :',
	'legend_personnalisation' => 'Персонализация',

	// S
	'supprimer' => 'Сбросить в настройки по умолчанию'
);

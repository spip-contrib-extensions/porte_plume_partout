<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-ppp?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'ppp_description' => 'Porte Plume všade sa používa na pridávanie panela úprav k poliam OPIS, PEREX a PS',
	'ppp_nom' => 'Porte Plume všade',
	'ppp_slogan' => 'Zobraziť Porte Plume pri väčšine textových polí'
);

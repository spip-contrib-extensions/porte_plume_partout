<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/ppp?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_mettre_a_jour' => 'Aktualizovať',

	// C
	'cfg_attention' => 'Pozor',
	'cfg_description' => 'uistite sa, že v šablónach máte povolené používanie Porte Plume iba pri poliach, kde  použitie skratiek nevyvolá chybu xhtml.',
	'cfg_titre' => 'Všade Porte Plume',

	// E
	'explication_personnalisation' => 'Zadajte cieľ objektov, ktoré budú používať typografický panel (výraz CSS alebo rozšírenie jQuery).',

	// L
	'label_hauteur_champ' => 'Polovičná výška obrazovky',
	'label_personnalisation' => 'Vybrať prispôsobené',
	'legend_barre_typo' => 'Typografický panel aktivovať na:',
	'legend_hauteur_champ' => 'Výška textového poľa pre články',
	'legend_personnalisation' => 'Prispôsobenie',

	// S
	'supprimer' => 'Obnoviť pôvodné hodnoty'
);
